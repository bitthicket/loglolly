namespace BitThicket.Logging.Façade

/// intended to be copy/pasted or fetched with paket.  feel free to change the namespace or this module name.  As
/// long as the internal modules, functions, and types remain unchanged, it should be fine.
module Logging =
    open System

    module private Time =
        [<Measure>] type ms

        let getCurrentTime () = DateTimeOffset.Now.ToUnixTimeMilliseconds() |> LanguagePrimitives.Int64WithMeasure

    type HierarchyId =
        { value : string }

    module private Hierarchy =
        let make str = { value = str }

    type LogLevel = 
        | Verbose
        | Debug
        | Info
        | Warn
        | Error
        | Fatal
        override this.ToString() = (this :> obj).ToString().ToLowerInvariant()

    type MetricValue=
        | Counter
        | Gauge
        | Histogram

    type Metric = 
        { value : MetricValue;
          name : string;
          labels : string array }

    type LogMessage = 
        { template : string
          fields : Map<string,obj>
          level : LogLevel }

    type Event = 
        { message : LogMessage option
          metrics : Metric list option
          timestamp : int64<Time.ms> }

    type ILogger =
        abstract member id : HierarchyId
        abstract member logEvent : Event -> Async<unit>

    [<AutoOpen>]
    module LoggerEx =
        type ILogger with
            member this.log level template fields =
                { message = Some { template = template; fields = fields; level = level };
                  metrics = None;
                  timestamp = Time.getCurrentTime() }
                |> this.logEvent
                |> Async.Start
            member this.verbose = this.log Verbose
            member this.debug = this.log Debug 
            member this.info = this.log Info
            member this.warn = this.log Warn
            member this.error = this.log Error
            member this.fatal = this.log Fatal

    type NullLogger() =
        interface ILogger with
            member this.id = { value = String.Empty }
            member this.logEvent event = async {
                ignore event
                ()
            }

    type Configuration =
        { getLogger : string -> ILogger }

    let private _defaultConfig =
        { getLogger = fun str  -> NullLogger() :> ILogger }

    let private _config = ref _defaultConfig
        
    let init cfg =
        _config := cfg